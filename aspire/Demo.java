package com.aspire;

import mm.sms.purchasesdk.OnSMSPurchaseListener;
import mm.sms.purchasesdk.SMSPurchase;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import kr.paleblue.bb.GameActivity;
import kr.paleblue.bb.RecordFee;
public class Demo extends Activity {
	/** Called when the activity is first created. */
	public static final int ITEM0 = Menu.FIRST;// 系统值
	public static final int ITEM1 = 2;
	private final String TAG = "Demo";

	public static SMSPurchase purchase;
	private Context context;

	private IAPListener mListener;

	// 计费信息 (现网环境)
	private static final String APPID = "300007650482";
	private static final String APPKEY = "85A03CA1E9866BAA";
	// 计费点信息
	private static final String LEASE_PAYCODE = "30000765048205";
	private static final int PRODUCT_NUM = 1;

	private String mPaycode;
	private int mProductNum = PRODUCT_NUM;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.main);
		Bundle bundle=getIntent().getExtras();
		String feeCode=bundle.getString("feeCode");
		if (feeCode.equalsIgnoreCase("001")) feeCode="30000765048205";
		 //  RecordFee.setPreference(GameActivity.getGameInstance(), "001",1);
		if (feeCode.equalsIgnoreCase("002")) feeCode="30000765048204";
		// RecordFee.setPreference(GameActivity.getGameInstance(), "002",1);
		if (feeCode.equalsIgnoreCase("003")) feeCode="30000765048206";
		// RecordFee.setPreference(GameActivity.getGameInstance(), "003",1);
		//30000765048204  	普通模式解锁  	购买类-不可重复购买  	2元  	  	 
		//30000765048206  	困难模式解
		//String mProductNum=bundle.getString("mProductNum");
		 android.util.Log.v("mytag","111:"+feeCode);
		 GameActivity.getGameInstance().purchase.smsOrder(GameActivity.getGameInstance(), feeCode,GameActivity.getGameInstance().mListener);
		// GameActivity.getGameInstance().purchase.
		this.finish();

	}

	
}